<?php

namespace localization\components;

use yii\base\Component;
use Yii;
use yii\helpers\Json;
use yii\httpclient\Client;
use yii\httpclient\CurlTransport;

/**
 * Класс для работы с локализациями
 *
 * Class LocalizationComponent
 * @package localization\components
 */
class LocalizationComponent extends Component
{
    /** @var string $url URL сервиса локализации */
    public $url = '';
    /** @var string $project_name Символьный идентификатор проекта */
    public $project_name = '';
    /** @var string $login Логин пользователя */
    public $login = '';
    /** @var string $password Пароль пользователя */
    public $password = '';
    /** @var string $authToken Токен авторизации */
    public $authToken = '';
    /** @var string $lang Текущий язык пользователя */
    public $lang = 'ru';
    /** @var array $langs Массив возможных языков */
    public $langs = ['ru', 'en'];
    /** @var string $directory папка где хранятся файлы */
    public $directory = null;
    /** @var bool $saveToFile Флаг надо ли сохранять переводы в файлы */
    public $saveToFile = false;

    /** @var int $durationLiveCash Время жизни кэша*/
    private static $durationLiveCash = 3600 * 24 * 7;


    /**
     * @var Client $client
     */
    public $client;

    /** @var string Тип запроса POST */
    const TYPE_REQUEST_POST = 'post';
    /** @var string Тип запроса GET */
    const TYPE_REQUEST_GET = 'get';



    public function init()
    {
        parent::init();
        $this->client = new Client([
            'transport' => CurlTransport::class,
        ]);
        $this->auth();
    }


    /**
     * Авторизация
     */
    public function auth()
    {
        $data = [
            'login' => $this->login,
            'password' => $this->password,
        ];

        $key = $this->url . 'v1/users/auth/auth' . json_encode($data);
        $result = Yii::$app->cache->get($key);
        if (empty($result)) {
            $result = $this->sendRequest(
                $this->url . '/v1/users/auth/auth',
                $data,
                self::TYPE_REQUEST_POST,
                true
            );
            Yii::$app->cache->set($key, $result, 3600 * 24);
        }

        if (!empty($result['token'])) {
            $this->authToken = $result['token'];
        }
    }


    /**
     * Проверить надо ли обновлять локализацию, если да, то обновить кэш локализаций проекта
     */
    public function checkAndUpdateLocalization()
    {
        if (!$this->checkStatusLocalizationProject()) {
            $this->updateLocalizations();
        }
    }


    /**
     * Сделать запрос в сервис и проерить актуальны ли переводы
     *
     * @return bool Вернёт true если всё актуально
     */
    private function checkStatusLocalizationProject()
    {
        $currentHashStatus = Yii::$app->cache->get('hash_localization_status_' . $this->project_name);
        $data = [
            'project_name' => $this->project_name,
            'hash' => empty($currentHashStatus) ? '' : $currentHashStatus,
        ];
        $url = $this->url . '/v1/localizations/check-status-project';
        $result = $this->sendRequest($url, $data);
        Yii::$app->cache->set(
            'hash_localization_status_' . $this->project_name,
            $result['hash'],
            self::$durationLiveCash
        );
        return $result['status'];
    }

    /**
     * Обновить метки проекта
     */
    private function updateLocalizations()
    {
        $localizations = [];
        $localizations = $this->getLocalizations($localizations, 0, 0);
        Yii::$app->cache->set(
            'localizations_project_' . $this->project_name,
            $localizations,
            self::$durationLiveCash
        );

        if ($this->saveToFile && !empty($this->directory)) {
            $this->saveToFile($localizations);
        }
    }

    private function saveToFile($localizations)
    {
        $directory = __DIR__ . '/../../../../../' . $this->directory . '/';
        $localizationsFromLanguage = [];
        foreach ($localizations as $localization) {
            foreach ($this->langs as $lang) {
                if (!isset($localizationsFromLanguage[$lang])) {
                    $localizationsFromLanguage[$lang] = [];
                }

                if (isset($localization[$lang])) {
                    $value = $localization[$lang];
                } else {
                    $value = $localization['slug'];
                }
                $keys = explode('.', $localization['slug']);
                $localizationsFromLanguage[$lang] = $this->parseLocalizationsToArray($localizationsFromLanguage[$lang], $keys, $value);
            }
        }
        foreach ($localizationsFromLanguage as $lang => $localizations) {
            $localizations = Json::encode($localizations);
            $file = $directory . $lang . '.json';
            $myfile = fopen($file, "w") or die("Unable to open file!");
            fwrite($myfile, $localizations);
            fclose($myfile);
        }
    }

    /**
     * Метод парсинга slug локали по ключам
     *
     * @param array $localizations массив переводов
     * @param array $keys ключи переводов
     * @param string $value перевод
     *
     * @return array вернёт массив с добавленой меткой
     */
    private function parseLocalizationsToArray($localizations, $keys, $value)
    {
        $key = $keys[0];
        unset($keys[0]);

        if (!isset($localizations[$key])) {
            if (!empty($keys)) {
                $localizations[$key] = [];
            } else {
                $localizations[$key] = $value;
            }
        }
        if (!empty($keys)) {
            $keys = array_values($keys);
            $localizations[$key] = $this->parseLocalizationsToArray($localizations[$key], $keys, $value);
        }

        return $localizations;
    }

    /**
     * Получить переводы по проекту
     *
     * @param array $localizations Массив локализаций
     * @param int $countLocalizations Количество уже полученных меток
     * @param int $page текущая страница
     *
     * @return array Вернёт массив полученых меток по проекту
     */
    private function getLocalizations($localizations, $countLocalizations, $page)
    {
        $data = [
            'project_name' => $this->project_name,
            'limit' => 1000,
            'page' => $page
        ];
        $url = $this->url . '/v1/localizations';
        $result = $this->sendRequest($url, $data, self::TYPE_REQUEST_GET);

        if (!empty($result)) {
            //Записываем результаты
            $countLocalizations += 1000;
            if (!empty($result['items'])) {
                $localizations = array_merge($localizations, $result['items']);
            }

            //Проверяем подтянули ли мы все локали или нет.
            if ($countLocalizations < (int)$result['count']) {
                $page++;
                $localizations = $this->getLocalizations($localizations, $countLocalizations, $page);
            }
        }

        return $localizations;
    }



    /**
     * Отправить запрос в сервис
     *
     * @param string $url URL запроса
     * @param array $data Данные запроса
     * @param string $typeRequest Тип запроса
     * @param bool $isAuth Является ли запрос авторизацией
     *
     * @return array|mixed
     */
    protected function sendRequest($url, $data, $typeRequest = self::TYPE_REQUEST_POST, $isAuth = false)
    {
        $request = $this->client
            ->createRequest()
            ->setMethod($typeRequest)
            ->setUrl($url);

        //В зависимости от типа надо добавить поля
        if ($typeRequest === self::TYPE_REQUEST_POST) {
            $request
                ->addHeaders(['content-type' => 'application/json'])
                ->setFormat(Client::FORMAT_JSON)
                ->setContent(Json::encode($data));
        } else {
            $request->setData($data);
        }

        //Если запрос не на авторизацию, то надо вставить токен
        if (!$isAuth) {
            $request
                ->addHeaders(['Authorization' => 'Bearer ' . $this->authToken]);
        }

        //Отпавляем запрос и получем ответ
        $response = $request->send();
        $result = $response->getData();

        if (isset($result['error']['code']) && $result['error']['code'] == 4404) {
            return [];
        }

        return $result['result'];
    }


    /**
     * Получить по символьному идентификатору и языку перевод
     *
     * @param string $slug Символьный идентификатор метки
     * @param array $parameters Переменные метки
     *
     * @return string Веренёт либо метку перевода, либо slug
     */
    public function getLocationBySlug($slug, $parameters = [])
    {
        $locations = Yii::$app->cache->get('localizations_project_' . $this->project_name);

        if (isset($locations[$slug])) {
            $location = $locations[$slug][$this->lang];
            foreach ($parameters as $key => $value) {
                $location = str_replace('{' . $key . '}', $value, $location);
            }
            return $location;
        }

        return $slug;
    }

    /**
     * Вернуть список докализаций проекта по нужному языку
     *
     * @return array Вернёт список локализаций
     */
    public function getLocalizationProject()
    {
        $locations = Yii::$app->cache->get('localizations_project_' . $this->project_name);
        $finalLocations = [];
        if (!empty($locations)) {
            foreach ($locations as $key => $location) {
                $finalLocations[$key] = $location[$this->lang];
            }
        }

        return $finalLocations;
    }

}