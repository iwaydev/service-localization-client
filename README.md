**Настройка компонента**

Пример конфига:

```
    'localization' => [
            'class' => localization\components\LocalizationComponent::class,
            'login' => 'TestProjectAdmin',
            'password' => 'idu183',
            'url' => 'http://api.localization.local:125/',
            'project_name' => 'test_project',
        ],
```
Где:
1) **class** - это Класс компонета
2) **login** - Логин пользователя проекта
3) **password** - Пароль пользователя
4) **url** - Адрес сервиса локализации
5) **project_name** - Символьный идентификатор проекта в сервисе локализации